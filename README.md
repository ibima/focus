# README #

## Overview ##

This R package supports the identification of those parts of a molecular network that
are the most characteristic for a disease. It is a three-stage process:

1. all nodes (vertices) and interactions (edges) of the network (graph) are scored by their single or paired log fold change
2. nodes and edges are remove from the network that are scored below a threshold/quantile value
3. eges may be reintroduced to avoid network fragmentation.

## How do I get set up? ##

### Installation ###

We suggest to install directly from the bitbucket repository:
``` .r
library(devtools)
devtools::install_bitbucket("ibima/focus")
```
### Retrieve a molecular network ###

To start you need to decicde about a basic network. The FocusHeuristic
reduces that network to those interactions that seem most relevant for
the changes observed in the transcriptome. We suggest the STRING network,
also shipping as an R package itself, to start from. Please install it
from BioConductor as described on 

``` .r
library(STRINGdb)
string.db <- STRINGdb$new( version="10", species=9606, score_threshold = 900 )
```
The STRINGdb is transformed
into an igraph object, which is also expected by the FocusHeuristics.

``` .r
StringEntrezGraph <- string.db$get_graph()
```

### Match gene/transcript/protein names of network with your data ###

The names and order of nodes need to match the order and names of
transcripts passed to evaluate the network. The algorithm is otherwise
agnostic about the exact naming convention. You may decide to follow
the mapping to the NCBI Entrez database or adapt the following lines
for your project at hand. Nodes to which no ID could be assigned, are
removed from the graph.

``` .r
ids = strsplit(V(StringEntrezGraph)$name, split = ".", fixed = T)
ids = cbind(unlist(lapply(ids, function(x) x[1])), unlist(lapply(ids, function(x) x[2])))
colnames(ids) = c("Taxon", "ENSP")
if(any(ids[,"Taxon"] != 9606))
  warning("Not only taxid ", 9606)
V(StringEntrezGraph)$name = ids[,"ENSP"]
db = "org.Hs.eg.db"
library(db, character.only=TRUE)
db = get(db)
key = "ENSEMBLPROT"
label = "ENTREZID"
suppressMessages( # Suppress: 'select()' returned 1:1 mapping between keys and columns
  ids.new <- mapIds(db, keys=unlist(ids[,"ENSP"]), column=label, keytype=key, multiVals="first")
)
```

We found out the hard way that the transformation to Entrez gene IDs is incomplete. We here
show a complementary approach with BioMart:

``` .r
library(biomaRt)
mart <- useMart(biomart = "ensembl", dataset = "hsapiens_gene_ensembl")
results <- getBM(attributes = c("ensembl_peptide_id","entrezgene"),
                 filters = "ensembl_peptide_id", 
                 values = names(ids.new)[is.na(ids.new)],
                 mart = mart)
ids.new[results[,"ensembl_peptide_id"]] <- results[,"entrezgene"]
rm(results)
```

We had spotted that deficiency while looking for the Cyclooxygenase that ASS inhibits. Now
this should be present:

``` .rconsole
> names(ids.new)[ids.new %in% c("5742","5743")]
[1] "ENSP00000354612" "ENSP00000356438"
```
This can be confirmed on the Ensembl web site. With the now much more complete assignment, all other nodes shall be removed:

```
V(StringEntrezGraph)$name = ids.new
StringEntrezGraph = delete_vertices(StringEntrezGraph, is.na(V(StringEntrezGraph)$name))
rm(ids.new)
```

### Retrieve expression data ###

The expression data are passed as a vector or log fold changes. The
logarithmic base is not relevant but should be consistent across all
genes. Any source of expression data will be suitable if the gene names
can be mapped to the names of nodes in the network.

A tantalizing dataset is the Connectivity Map of changes induced by drugs
on the transcriptome. Our group provides an R package with that data at
https://bitbucket.org/ibima/moldrugeffectsdb. Most users of the FocusHeuristics
are likely to work on their own expression data. However, to follow this
example, please again perform an installation with devtools:

``` .r
devtools::install_bitbucket("ibima/moldrugeffectsdb",ref="masterWithData")
```

Find a drug of interest from the columns of the matrix "CMap" as provided
by the MolDrugEffectsDB package:

``` .r
library(MolDrugEffectsDB)
data(CMap)
for(platform in names(CMap)) {
  cat("CMap platform: ",platform,"\n",sep="")
  print(grep(x=colnames(CMap[[platform]]),pattern="acetylsalicylic acid",value=TRUE))
}
```

This will yield the following overview on tests of Aspirin on four different cell lines in the same concentration:

  CMap platform: U133AAofAv2
  [1] "acetylsalicylic acid_1e-04_MCF7"
  CMap platform: HG-U133A
  character(0)
  CMap platform: HT_HG-U133A
  [1] "acetylsalicylic acid_1e-04_HL60" "acetylsalicylic acid_1e-04_MCF7"
  [3] "acetylsalicylic acid_1e-04_PC3"

``` .r
head(CMap[[3]][,"acetylsalicylic acid_1e-04_HL60","logFoldChange"],18)
```

### Matching expression data with the network ###

The CMap data is already offered with Entrez identifiers:

``` .rconsole
            10          100         1000        10000        10001        10002
   0.039777234 -0.078087347  0.047266250  0.045881366 -0.096430998 -0.019518445
         10003        10004        10005        10006        10007        10009
   0.002597088  0.022031611  0.187980996  0.059913392  0.027991961  0.094005923
          1001        10010    100126791    100127972    100128124    100128329
   0.166822727  0.099400328 -0.188520590  0.110702446 -0.112697968  0.004934500
```

To have a perfect mapping between network and transcript list, a subset is nodes
that is shared between network and transcriptome is to be determined:

``` .r
names.shared <- names(V(StringEntrezGraph))[names(V(StringEntrezGraph)) %in% rownames(CMap[[3]])]
net <- delete_vertices(StringEntrezGraph, !V(StringEntrezGraph)$name %in% names.shared)
rm(names.shared)
```

The expresson data are passed as absolute values to the FocusHeuristics. If these
are not available, one cane alternatively only use the log fold change and cbind with 0.

``` .r
x <- cbind(treated=CMap[[3]][,"acetylsalicylic acid_1e-04_HL60","logMean"],
           control=CMap[[3]][,"acetylsalicylic acid_1e-04_HL60","logMean"]
                    -
                  CMap[[3]][,"acetylsalicylic acid_1e-04_HL60","logFoldChange"])
```

Quick inspection, to confirm that lengths fit:

``` .rconsole
> sum(rownames(x) %in% names(V(net)))
[1] 11474
```
The exact number of matching IDs depends on the threshold passed to STRING
for the retrieval of the network. The FocusHeuristics::focus() function
matches and selects on the name
attributes. Other nodes/transcripts are ignored. We found it to be a good practice to manually inspect that
everything is named as expected and that the correspondence between
transcritome and network is established as expected.

### Invocation of the FocusHeuristics ###

With this, we are now set to invoke the network reduction. The execution time
depends on the number of edges in the network. Expect it to be less than five
minutes.

``` .r
library(FocusHeuristics)
net.focus <- focus(x,xTrmtIdx=1,netGraph=net)
```

With these default parameters, no reduction of the network is performed.
We can use this initial setup to inspect the edge annotation and search for
a priori knowledge. To stay with our aspirin example, we could for instance
inspect transcripts with known association to COX or prostaglandins, say
because of a feedback loop sensing a disturbance on that metabolic inhibition.
The quantile of scores associated with those transcripts shall then be
used as threshold:

``` .rconsole
> sapply(c("5742","5743"), function(entrezid) {
c(
  LS.upp=max(net.focus$edge.scores[net.focus$edge.scores[,1] == entrezid,"scores.LS"]),
  IS.upp=max(net.focus$edge.scores[net.focus$edge.scores[,1] == entrezid,"scores.IS"]),
  LS.low=min(net.focus$edge.scores[net.focus$edge.scores[,1] == entrezid,"scores.LS"]),
  IS.low=min(net.focus$edge.scores[net.focus$edge.scores[,1] == entrezid,"scores.IS"])
)
})
             5742       5743
LS.upp  0.3667975  0.5305868
IS.upp 21.1317479 21.6456904
LS.low -0.1669072 -0.2165588
IS.low 11.0751559 11.1689013
```

```.rconsole

# link scores

> quantile(net.focus$edge.scores[,"scores.LS"],probs=0:20/20)
           0%            5%           10%           15%           20%
-0.7571504116 -0.1897731167 -0.1425974070 -0.1132692951 -0.0908858547
          25%           30%           35%           40%           45%
-0.0722886823 -0.0559649827 -0.0411399455 -0.0271616160 -0.0137850417
          50%           55%           60%           65%           70%
-0.0005032347  0.0128081235  0.0265201719  0.0409337859  0.0564782258
          75%           80%           85%           90%           95%
 0.0737090084  0.0932945855  0.1168469342  0.1479768381  0.1970062318
         100%
 0.7550358198
 
# interaction scores
 
> quantile(net.focus$edge.scores[,"scores.IS"],probs=0:20/20)
       0%        5%       10%       15%       20%       25%       30%       35%
 8.059313  9.088093  9.374021  9.725573 10.262819 10.913982 11.517967 12.087360
      40%       45%       50%       55%       60%       65%       70%       75%
12.632895 13.159747 13.662761 14.170979 14.697961 15.273715 15.911048 16.598901
      80%       85%       90%       95%      100%
17.385209 18.258004 19.365405 21.061209 29.375501

```

### Parameter optimisation ###

See how many interactions have a stronger score than the one we already know to be important.

```.rconsole
> quantile.upp <- sum(0.36<net.focus$edge.scores[,"scores.LS"])/length(E(net))
> quantile.upp
[1] 0.003274114
```
That are not too many, now reducing the network to those edges:

``` .r
net.focus.min <- focus(x,xTrmtIdx=1,netGraph=net,ls.q.low=0, ls.q.upp=quantile.upp)
```
We do not want to print or even interpret a graph with 11000 nodes. This should be reduced to smaller subgraphs. We propose to look at
 * the largest subgraph of connected compounds
 * the largest subgraph that contains one of our target genes
The higher the threshold is that connects genes in STRING, the larger is the fragmentation of the network.

``` .r
giant.component <- function(graph, targets=NULL) {
  cl <- clusters(graph, ...)
  induced_subgraph(graph, which(cl$membership == which.max(cl$csize)))
}
```

Also, one may sequentially look at those subgraphs that contain a gene of particular interest.

### See also ###

Mathias Ernst, Yang Du, Gregor Warsow, Mohamed Hamed, Nicole Endlich, Karlhans Endlich, Hugo Murua Escobar,
Lisa-Madeleine Sklarz, Sina Sender, Christian Junghan�, Steffen M�ller, Georg Fuellen & Stephan Struckmann
"FocusHeuristics � expression-data-driven network optimization and disease gene prediction"
Scientific Reports 7, Article number: 42638 (2017)
doi:10.1038/srep42638
https://www.nature.com/articles/srep42638
    
### Contribution / Questions ###

* Please contact stephan.struckmann@uni-rostock.de or steffen.moeller@uni-rostock.de
