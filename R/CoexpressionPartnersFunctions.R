f.FilterByVariance = function (xx,NetworkGraph,var.quantile) {
  no.variance = apply( xx , 1 , var ) == 0
  xx = xx[ !no.variance , ]
  y = rownames(xx) %in% V(NetworkGraph)$name
  xx.not.network = xx[ ! y , ]
  xx.network = xx[ y ,]
  
  xx.var = apply( xx.not.network , 1 , var)
  xx.not.network = xx.not.network[ xx.var > quantile( xx.var , var.quantile ) , ]
  xx.new = rbind( xx.network , xx.not.network )
  return(xx.new)
}

f.ConstructCoexpressionGraph = function (xx , cor.cutoff = 0.7 ) {
  xx.cor = cor( t(xx) ) ** 2
  diag(xx.cor) = 0
  xx.cor[ xx.cor < cor.cutoff ] = 0 
  
  G.coexp = graph.adjacency(xx.cor , weighted = T )
  G.coexp = igraph::simplify( as.undirected(G.coexp) )
  E(G.coexp)$weight = E(G.coexp)$weight / 2
  G.coexp
  
  list( cor.matrix = xx.cor , cor.graph = G.coexp )
} 


f.CollapseWeights = function (g) {
  ww = cbind( E(g)$weight_1 , E(g)$weight_2 )
  E(g)$weight = apply( ww , 1 , max , na.rm = T )
  g
}

f.GetMetaNode = function (G,NetworkGraph,cl,i,xx.cor) { 

  meta.node.name = paste( i , "metanode" , sep=".")
  cluster.nodes = V(NetworkGraph)[cl$membership == i ]$name
  outside.nodes = V(G)[ ! V(G)$name %in% cluster.nodes ]$name
  
  ij.matrix = xx.cor[ rownames(xx.cor) %in% cluster.nodes , colnames(xx.cor) %in% outside.nodes , drop=F ]
  best.targets = apply( ij.matrix , 2 , max )
  best.targets.index = apply( ij.matrix , 2 , which.max )
  
  if ( length(best.targets.index) > 0 ) {
    targets.df = data.frame( target.gene = colnames(ij.matrix) , cluster.gene = rownames(ij.matrix)[best.targets.index] ,
                             meta.node = rep( meta.node.name , ncol( ij.matrix ) ) , 
                             weight = best.targets ,stringsAsFactors = F)
    return(targets.df)
  } else { return(NA) }
}
  

f.GetShortestPaths = function (G, metanode.df) {
  meta.nodes = unique( metanode.df$meta.node )
  M = matrix( 0 , ncol=length(meta.nodes) , nrow=length(meta.nodes) )
  rownames(M) = colnames(M) = meta.nodes
  for ( i in meta.nodes ) {
    for ( j in meta.nodes ) {
      if ( i != j ) {
        my.path = get.shortest.paths( G , from = i , to = j , mode="all" , output="both")
        my.weights = E(G)[ unlist(my.path$epath) ]$weight
        M[ i , j ] = sum(my.weights)
        M[ j , i ] = sum(my.weights)
    }
  }
  }
  M
}

#f.GetShortestPathsParallel = function (G,metanode.df , num.of.clusters = detectCores() ) {
#  meta.nodes = unique( metanode.df$meta.node )
#  M = matrix( 0 , ncol=length(meta.nodes) , nrow=length(meta.nodes) )
#  rownames(M) = colnames(M) = meta.nodes
#  my.c = combn( meta.nodes , 2 )

#  cl.par = makeCluster(num.of.clusters)
#  clusterExport(cl.par , c("G","my.c","get.shortest.paths","E") , envir = environment() )

#  res = parApply( cl.par , my.c , 2 , function (a) {
#    my.path = get.shortest.paths( G , from = a[1] , to = a[2] , mode="all" , output="both")
#    my.weights = E(G)[ unlist(my.path$epath) ]$weight
#    sum(my.weights)
#  })
  
#  stopCluster(cl.par)
#  for ( i in 1:ncol(my.c) ) {
#    M[ my.c[1,i] , my.c[2,i] ] = res[i]
#    M[ my.c[2,i] , my.c[1,i] ] = res[i]
#  }
#  M
#}




f.ExpandMST = function (mst.obj,G) {
  mst.edges = get.edgelist(mst.obj)
  mst.paths = lapply( 1:nrow(mst.edges) , function (i) {
    x = mst.edges[i,]
    my.path = get.shortest.paths( G , from = x[1] , to = x[2] , mode="all" , output="both")
    path.nodes = V(G)[unlist(my.path$vpath)]$name 
    path.nodes
  })
  mst.paths
}

f.DeconvoluteMetaNodes = function (ll,metanode.df) {
  pair.1 = ll[c(1,2)]
  pair.2 = ll[c(length(ll)-1,length(ll))]
  pair.1.cluster.gene = metanode.df$cluster.gene[ metanode.df$target.gene == pair.1[2] & metanode.df$meta.node == pair.1[1] ]
  pair.2.cluster.gene = metanode.df$cluster.gene[ metanode.df$target.gene == pair.2[1] & metanode.df$meta.node == pair.2[2] ]
  
  my.edges = rbind( c( pair.1.cluster.gene , pair.1[2] ) ,
                    c( pair.2.cluster.gene , pair.2[1] ) )
  if ( length(ll) > 3 ) { 
    for ( i in 2: (length(ll)-2) ) {
      my.edges = rbind( my.edges , c( ll[i] , ll[i+1]) )
    }
  }
  g.ij = igraph::simplify( graph.edgelist(my.edges , directed = F) )
  g.ij
}

f.ExprEssenceColoring = function (g,x,xTrmtIdx) {
  
  x.means = cbind( rowMeans( x[ , -xTrmtIdx , drop=F ]) , rowMeans( x[ , xTrmtIdx , drop=F ] ) )
  colnames(x.means) = c("control","treatment")
  
  f.col = colorRampPalette( c("darkgreen","green","yellow","red","darkred"))
  
  x.vec = as.numeric( x.means )
  names(x.vec) = 1:length(x.vec)
  
  x.col = f.col( length(x.vec) )
  x.vec = sort( x.vec )
  names(x.col) = names(x.vec)
  
  x.col = x.col[ order( as.numeric(names(x.col)))]
  x.col = matrix(x.col,ncol=2,byrow = F)
  rownames(x.col) = rownames(x)
  
  if ( all( x.means[,"control"] == 0 ) ) {
    x.col[,1] = rep( rgb(1,1,1) , nrow(x.col) )
  } 
  
  V(g)$shape = "pie"
  V(g)$pie = lapply(1:vcount(g), function (i) c(1,1) ) 
  V(g)$pie.color = lapply( V(g)$name , function (gene) { x.col[gene,] } )
  
  return(g)
}



plot.Similarities = function (sim.res , focus.res , i.from.top=1 ) { 
  
  A = sim.res$correlations
  A = A[ order( A$cor , decreasing = T ) , ]
  
  drug.set = unlist(A[ i.from.top , 1:sim.res$num.of.drugs ])
  
  y.LS.focus = sim.res$yFocused
  focus.df = sim.res$focusTable
  
  dat = data.frame( focus.df$LinkScore , y.LS.focus[ , drug.set ] )
  names(dat)[1] = "Y"
  
  cc.res = cancor( dat[,1] , dat[,2:ncol(dat)] )
  
  a = t( cc.res$ycoef ) %*% t( dat[,2:ncol(dat)] )
  
  dat2 = dat
  dat2$LinComb = a[1,]
  
  pairs( as.matrix( dat2) )
  
}

focus2similarity = function (res.focus,y.scores,combo.level=1,...) {
  
  y.LS = y.scores$yE
  
  focus.df = as.data.frame( get.edgelist( res.focus$netGraph ) , stringsAsFactors = F )
  rownames(focus.df) = apply( focus.df , 1 , paste , collapse = "." )
  focus.df$LinkScore = E(res.focus$netGraph)$LinkScore
  focus.df = focus.df[ ! is.na(focus.df$LinkScore) , ]
  
  y.LS.focus = y.LS[ rownames(focus.df) , ]
  
  L.single = apply( y.LS.focus , 2 , function (my.col) {
    sign( cor( focus.df$LinkScore , my.col  ) )
  })
  
  combos = combn( colnames(y.LS.focus) , combo.level )
  
  combos.sign = combn( L.single , combo.level )
  
  has.same.sign = apply( combos.sign , 2 , function (z) { all( z == 1 ) } ) 
  
  combos = combos[ , has.same.sign ]
  
  LS.correlations = sapply( 1:ncol(combos) , function (i)  {
    dat = data.frame( focus.df$LinkScore , y.LS.focus[ , combos[ , i ] ] )
    names(dat) = c("Y",combos[ , i ] )
    lm1 = lm( Y ~ . , data = dat )
    summary(lm1)$r.squared
  })
  
  names(LS.correlations) = apply( combos , 2 , paste , collapse="." )
  LS.correlations.df = as.data.frame( t(combos) , stringsAsFactors = F )
  LS.correlations.df$drug.set = names(LS.correlations)
  LS.correlations.df$cor = LS.correlations 
  
  
  list( "correlations" = LS.correlations.df,"focusTable" = focus.df,"focusGraph" = res.focus$netGraph , "yFocused" = y.LS.focus,
        "num.of.drugs" = combo.level ) 
  
}



f.ConvertGeneNames = function (g) {
### still missing  
}  

