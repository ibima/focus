Package: FocusHeuristics
Type: Package
Title: Find most explanatory molecular sub-network for differentially expressed genes
Version: 1.04
Date: 2017-09-08
Author: Stephan Struckann, Yang Du, Steffen Moeller, Georg Fuellen, Mathias Ernst
Maintainer: Stephan Struckmann, Mathias Ernst <ibima-sekretariat@uni-rostock.de>
Description: This package aims at identifying a parts of a molecular network that
 seems most characteristic for a disease. It is a two-stage process that sees
 genes and their interactions first removed from the network. Respective thresholds
 are applied on scores that are derived from log fold changes of an aoccompanying
 gene expression dataset.
 In an optional second stage, eges may also be reintroduced to avoid network
 fragmentation.
License: GPL (>= 2)
LazyLoad: yes
Depends: R (>= 2.10), igraph, gridBase, gridExtra
Suggests: testthat
Imports: qvalue, parallel, plyr, stats, grDevices, utils, graphics, AnnotationDbi
Encoding: UTF-8
